//
//  AppDelegate.h
//  Touch
//
//  Created by 209crc on 2015/02/28.
//  Copyright (c) 2015年 209crc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

