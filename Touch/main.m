//
//  main.m
//  Touch
//
//  Created by 209crc on 2015/02/28.
//  Copyright (c) 2015年 209crc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
